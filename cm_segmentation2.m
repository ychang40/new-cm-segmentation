function cm_segmentation2(varargin)
clear all; close all;

global handles

% initial variables
colors = ['y','r','b','c','g','w','m'];


fig1=figure('Position',[200 200 200 150],'MenuBar','none');

pb1=uicontrol('Style','pushbutton','String','Load Images','Position',...
    [0 100 100 50],'Callback',@load_imageset);

pb2=uicontrol('Style','pushbutton','String','Create Layer','Position',...
    [0 50 100 50],'Callback',@create_layer);

pb3=uicontrol('Style','pushbutton','String','Save','Position',...
    [0 0 100 50],'Callback',@save_segmentation);

pb4=uicontrol('Style','pushbutton','String','Remove Last Layer','Position',...
    [100 0 100 50],'Callback',@remove_layer);

pb5=uicontrol('Style','pushbutton','String','Jump','Position',...
    [100 50 100 50],'Callback',@jump);

pb6=uicontrol('Style','pushbutton','String','Load Seg...','Position',...
    [100 100 100 50],'Callback',@load_segmentation);

    function load_segmentation(hObject, eventdata)
        
        [FileName,FilePath] = uigetfile('*mat');
        
%         keyboard;
        
        load([FilePath FileName]);
%         close gcf
%         close gcf
        
        [PathName,~,ext]=fileparts(handles.path);
        
        PathName=[PathName '\'];
        files=dir([FilePath  '*bmp' ]);
        
        for i=1:length(files)
%             keyboard;
            handles.imCube(:,:,i)=mat2gray(imread([FilePath files(i).name]));
        end
        
        % file description
        handles.opened=datestr(now);
        handles.path=[PathName FileName];
        
        handles.fig=figure('units','normalized','outerposition',[0 0 0.9 0.9],'position',[0 0 0.9 0.9],'WindowScrollWheelFcn',@scroll_listen);
        handles.imhandle=imagesc(handles.imCube(:,:,handles.index));colormap(gray); axis off;
        set(handles.imhandle,'ButtonDownFcn',@key_listen);
        set(handles.fig,'name',['Image 1/' num2str(size(handles.imCube,3)) ', layer ' num2str(handles.layerIndex)]);
         
    end


    function jump(hObject, eventdata)
        
        answer = inputdlg('Jump to...');
        frame=str2num(answer{1});
        
        if frame>=1 || frame<=size(handles.imCube,3)
            
%            keyboard;
            handles.index=frame;
            set(handles.fig,'name',['Image ' num2str(handles.index)  '/' num2str(size(handles.imCube,3)) ', layer ' num2str(handles.layerIndex)]);
            handles.imhandle=imagesc(handles.imCube(:,:,handles.index),'Parent',get(handles.fig,'Children')); colormap(gray); 
            
            set(get(handles.fig,'Children'),'Visible','off');
            set(handles.imhandle,'ButtonDownFcn',@key_listen);
            plot_present;
            
        end
    end

    function save_segmentation(hObject, eventdata)
        handles.imCube=[];
        handles.fig=[];
        handles.imhandle=[];
        uisave('handles');
    end

    function load_imageset(hObject, eventdata)
        
        handles=struct;
        
        [FileName,PathName,FilterIndex] = uigetfile({'*.bmp';'*.tiff';'*.png';'*.*'});
        
        [~,~,ext]=fileparts(FileName);
        
        files=dir([PathName  '*' ext]);
        
        for i=1:length(files)
            handles.imCube(:,:,i)=mat2gray(imread([PathName files(i).name]));
        end
        
        % file description
        handles.opened=datestr(now);
        handles.path=[PathName FileName];
        
        handles.fig=figure('units','normalized','outerposition',[0 0 0.9 0.9],'position',[0 0 0.9 0.9],'WindowScrollWheelFcn',@scroll_listen);
        handles.imhandle=imagesc(handles.imCube(:,:,1));colormap(gray); axis off;
        set(handles.imhandle,'ButtonDownFcn',@key_listen);
        set(handles.fig,'name',['Image 1/' num2str(size(handles.imCube,3)) ]);
        
        % initialize parameters
        handles.layer=1;
        handles.layerIndex=1;
        handles.index=1;
        
        
    end

    function create_layer(hObject, eventdata)
         stringsbutton = questdlg('Add new layer?');
         if strcmp(stringsbutton, 'Yes')
            handles.layer=[handles.layer handles.layer(end)+1];
         end
    end

    function remove_layer(hObject, eventdata)
        stringsbutton = questdlg('Remove last layer?');
        if strcmp(stringsbutton, 'Yes') && length(handles.layer)>1
            for i=1:length(handles.im)
                try
                    handles.im(i).layer(length(handles.layer))=[];
                catch
                end
            end
        end
    end


    function plot_present()
%        keyboard;
        for i=1:length(handles.layer)
            try
%                axes(handles.handles.imhandle);
                hold on;
                h=plot(handles.im(handles.index).layer(handles.layer(i)).fitPts(1,:),...
                    handles.im(handles.index).layer(handles.layer(i)).fitPts(2,:),'LineWidth',2,'color', colors(i));
                handles.im(handles.index).layer(handles.layer(i)).h=h;
            catch
            end
        end
       hold off; 
    end


    function scroll_listen(hObject, eventdata)
        if eventdata.VerticalScrollCount < 0 && handles.index~=1
            get(handles.fig); 
            handles.index=handles.index-1;
            set(handles.fig,'name',['Image ' num2str(handles.index)  '/' num2str(size(handles.imCube,3))]);
            handles.imhandle=imagesc(handles.imCube(:,:,handles.index));colormap(gray); axis off;
            
            set(handles.imhandle,'ButtonDownFcn',@key_listen);
            plot_present;

        elseif eventdata.VerticalScrollCount > 0 && handles.index~=size(handles.imCube,3)
            get(handles.fig); 
            handles.index=handles.index+1;
            set(handles.fig,'name',['Image ' num2str(handles.index)  '/' num2str(size(handles.imCube,3))]);
            handles.imhandle=imagesc(handles.imCube(:,:,handles.index));colormap(gray); axis off;
            
            set(handles.imhandle,'ButtonDownFcn',@key_listen);
            plot_present;
        end
        
    end

    function key_listen(hObject, eventdata)
        
%         keyboard;
        str=get(gcf,'SelectionType');
%         num=str2num(str);
        disp(str);
        
        switch str
            case 'extend'
                
                try
                    delete(handles.im(handles.index).layer(handles.layer(handles.layerIndex)).h);
                catch
                end
                [xi,yi,fitx,fity,h]=make_boundary(colors(handles.layerIndex));
                
%                 if 
%                     keyboard;
%                 end
                
                handles.im(handles.index).layer(handles.layer(handles.layerIndex)).actualPts=[xi; yi];
                handles.im(handles.index).layer(handles.layer(handles.layerIndex)).fitPts=[fitx; fity];
                handles.im(handles.index).layer(handles.layer(handles.layerIndex)).h=h;
                
            case 'open'
                if handles.layerIndex~=length(handles.layer)
                    handles.layerIndex=handles.layerIndex+1;
                    
                    str=get(handles.fig,'name');
                    set(handles.fig,'name',['Image ' num2str(handles.index)  '/' num2str(size(handles.imCube,3)) ', layer ' num2str(handles.layerIndex)]);
                else
                    handles.layerIndex=1;
                    
                    str=get(handles.fig,'name');
                    set(handles.fig,'name',['Image ' num2str(handles.index)  '/' num2str(size(handles.imCube,3)) ', layer ' num2str(handles.layerIndex)]);
                end
        end
    end

    function [xi,yi,fitx,fity,h]=make_boundary(color)
        [xi,yi,~]=impixel;
        
        fitx=[min(xi):max(xi)];
        fity=interp1(xi,yi,fitx,'cubic','extrap');
        
        hold on;
        get(handles.fig);
        h=plot(fitx,fity,'LineWidth',2,'Color',color);
        
    end
end
